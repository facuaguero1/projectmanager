from django.contrib import admin
from .models import *

# Register your models here.


admin.site.register(Project)
admin.site.register(Collection)
admin.site.register(Currency)
admin.site.register(PriceList)
admin.site.register(PriceListDetail)
admin.site.register(Company)
admin.site.register(Client)
admin.site.register(Resource)
admin.site.register(ResourceType)
admin.site.register(Person)
admin.site.register(LabourBreakdown)