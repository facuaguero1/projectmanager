# Generated by Django 2.0.4 on 2018-05-07 20:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20180507_1458'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='collection',
            name='currency',
        ),
    ]
