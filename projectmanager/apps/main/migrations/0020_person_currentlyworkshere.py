# Generated by Django 2.0.5 on 2018-07-12 21:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20180712_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='currentlyWorksHere',
            field=models.BooleanField(default=True),
        ),
    ]
