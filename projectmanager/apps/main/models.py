from django.db import models
from datetime import *

# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=40, null=True)
    company = models.ForeignKey('Company', on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey('Client', on_delete=models.SET_NULL, null=True)
    dealDate = models.DateField(null=True)
    salesPrice = models.FloatField(default=0.0)
    priceList = models.ForeignKey('PriceList', on_delete=models.SET_NULL, null=True)
    exchangeRate = models.FloatField(default=1.0)
    otherCosts = models.FloatField(default=0.0)


    def __str__(self):
        return self.name


    def calculateProjectedProfit(self):
        salesPrice = self.getSalesPrice()
        if self.getPriceList().getCurrency().getAcronym() == "USD":
            salesPrice *= self.getExchangeRate()
        projectedProfit = salesPrice - self.calculateTaxes() - self.calculateCostARS()
        return projectedProfit


    def calculateNetProfit(self):
        netProfit = 0
        settlement = self.calculateSettlement()
        if settlement != 0:
            netProfit = settlement - self.calculateTaxes() - self.calculateCostARS()
        return netProfit



    def calculateCostARS(self):
        labourBreakdownElements = LabourBreakdown.objects.filter(project=self)
        total = 0
        for element in labourBreakdownElements:
            total += element.calculateFinalCostARS()
        
        total += self.otherCosts
        return total


    def calculateCollected(self):
        collections = self.collection_set.all()
        
        collected = 0
        for c in collections:
            collected += c.getAmount()
        
        return collected


    def calculatePending(self):
        pending = self.salesPrice - self.calculateCollected()

        return pending


    def calculateSettlement(self):
        settlement = 0
        if self.getPriceList().getCurrency().getAcronym() == "USD":
            collections = self.collection_set.all()
            for c in collections:
                settlement += c.settlementARS()
        else:
            collected = self.calculateCollected()
            if collected > 0:
                settlement = collected

        return settlement


    def calculatePending(self):
        return self.salesPrice - self.calculateCollected()


    def calculateTaxes(self):
        collections = self.collection_set.all()
        taxes = 0

        for c in collections:
            taxes += c.calculateTotalTaxes()

        return taxes


    #Getters

    def getCompany(self):
        return self.company


    def getClient(self):
        return self.client
        

    def getDealDate(self):
        return self.dealDate
        

    def getSalesPrice(self):
        return self.salesPrice
        

    def getPriceList(self):
        return self.priceList
        

    def getExchangeRate(self):
        return self.exchangeRate


    #Setters

    def setName(self, arg):
        try:
            self.name = str(arg)
            self.save()
        except:
            pass
                        

    def setCompany(self, arg):
        if type(arg) is Company:
            self.company = arg
            self.save()
                        

    def setClient(self, arg):
        if type(arg) is Client:
            self.client = arg
            self.save()
                        

    def setDealDate(self, arg):
        try:
            year = int(arg[6:])
            month = int(arg[0:2])
            day = int(arg[3:5])
            self.dealDate = date(year, month, day)
            self.save()
        except Exception:
            print(Exception)


    def setSalesPrice(self, arg):
        try:
            self.salesPrice = float(arg)
            self.save()
        except:
            pass


    def setPriceList(self, arg):
        if type(arg) is PriceList:
            self.priceList = arg
            self.save()


    def setExchangeRate(self, arg):
        try:
            self.exchangeRate = float(arg)
            self.save()
        except:
            pass     



class Collection(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, null=True)
    cEntity = models.CharField(max_length=40, null=True)
    settlementDate = models.DateField(null=True)
    amount = models.FloatField(default=0.0)
    exchangeRate = models.FloatField(default=1.0)
    sourceCommision = models.FloatField(default=0.0)
    commisionARS = models.FloatField(default=0.0)
    taxesARS = models.FloatField(default=0.0)


    def __str__(self):
        return str(self.settlementDate) + ' - ' + str(self.project)


    #Getters

    def getProject(self):
        return self.project


    def getCEntity(self):
        return self.cEntity


    def getSettlementDate(self):
        return self.settlementDate


    def getAmount(self):
        return self.amount


    def getExchangeRate(self):
        return self.exchangeRate


    def getSourceCommision(self):
        return self.sourceCommision


    def getCommisionARS(self):
        return self.commisionARS


    def getTaxesARS(self):
        return self.taxesARS 


    def currency(self):
        return self.project.priceList.currency


    def settlementUSD(self):
        settlement = self.amount
        if self.currency().acronym == "USD":
            settlement -= self.sourceCommision
        
        return settlement


    def settlementARS(self):
        settlement = self.settlementUSD() * self.exchangeRate
        settlement -= ( self.getCommisionARS() + self.getTaxesARS() )
        return settlement


    def calculateRentas(self):
        rentas = 0
        if self.currency().getAcronym() == "USD" and self.getCEntity() == "PROICERE":
            rentas = self.settlementARS() * 0.04
        elif self.currency().getAcronym() == "ARS":
            rentas = self.getAmount() * 0.04
        else:
            rentas = 0.0

        return rentas


    def calculateMunicipales(self):
        municipales = 0
        if self.currency().getAcronym() == "USD" and self.getCEntity() == "PROICERE":
            municipales = self.settlementARS() * 0.01
        elif self.currency().getAcronym() == "ARS":
            municipales = self.amount * 0.01
        else:
            municipales = 0.0

        return municipales


    def calculateImpuestoCheque(self):
        impcheque = 0
        if self.currency().getAcronym() == "USD" and self.getCEntity() == "PROICERE":
            impcheque = self.settlementARS() * 0.006
        elif self.currency().getAcronym() == "ARS":
            impcheque = self.amount * 0.006
        else:
            impcheque = 0.0
            
        return impcheque


    def calculateTotalTaxes(self):
        return self.calculateRentas() + self.calculateMunicipales() + self.calculateImpuestoCheque()


    #Setters


    def setExchangeRate(self):
        self.exchangeRate = self.currency().rate


    def setProject(self, arg):
        if type(arg) is Project:
            self.project = arg
            self.save()
    

    def setCEntity(self, arg):
        self.cEntity = str(arg)
        self.save()

    
    def setSettlementDate(self, arg):
        try:
            year = int(arg[6:])
            month = int(arg[0:2])
            day = int(arg[3:5])
            self.settlementDate = date(year, month, day)
            self.save()
        except:
            pass


    def setAmount(self, arg):
        try:
            self.amount = float(arg)
            self.save()
        except:
            pass


    def setExchangeRate(self, arg):
        try:
            self.exchangeRate = float(arg)
            self.save()
        except:
            pass


    def setSourceCommision(self, arg):
        try:
            self.sourceCommision = float(arg)
            self.save()
        except:
            pass


    def setCommisionARS(self, arg):
        try:
            self.commisionARS = float(arg)
            self.save()
        except:
            pass


    def setTaxesARS(self, arg):
        try:
            self.taxesARS = float(arg)
            self.save()
        except:
            pass


class Currency(models.Model):
    name = models.CharField(max_length=30, unique=True)
    acronym = models.CharField(max_length=5, unique=True)
    rate = models.FloatField()


    def __str__(self):
        return self.acronym


    #Getters


    def getName(self):
        return self.name


    def getAcronym(self):
        return self.acronym


    def getRate(self):
        return self.rate


class PriceList(models.Model):
    name = models.CharField(max_length=30, unique=True)
    description = models.CharField(max_length=50)
    currency = models.ForeignKey('Currency', on_delete=models.SET_NULL, null=True)
    startDate = models.DateField(null=True)
    endDate = models.DateField(null=True)


    def __str__(self):
        return self.name


    def __eq__(self, other):
        return type(other) == PriceList and self.name == other.getName()


    #Getters


    def getName(self):
        return self.name


    def getDescription(self):
        return self.description


    def getCurrency(self):
        return self.currency


    def getStartDate(self):
        return self.startDate


    def getEndDate(self):
        return self.endDate


    #Setters


    def setName(self, arg):
        self.name = str(arg)
        self.save()


    def setDescription(self, arg):
        self.description = str(arg)
        self.save()


    def setCurrency(self, arg):
        if type(arg) is Currency:
            self.currency = arg
            self.save()


    def setStartDate(self, arg):
        try:
            year = int(arg[6:10])
            month = int(arg[0:2])
            day = int(arg[3:5])
            self.startDate = date(year, month, day)
            self.save()
        except:
            pass


    def setEndDate(self, arg):
        try:
            year = int(arg[6:])
            month = int(arg[0:2])
            day = int(arg[3:5])
            self.endDate = date(year, month, day)
            self.save()
        except:
            pass


class PriceListDetail(models.Model):
    priceList = models.ForeignKey(PriceList, on_delete=models.CASCADE, null=True)
    resourceType = models.ForeignKey('ResourceType', on_delete=models.CASCADE, null=True)
    cost = models.FloatField(default=0.0)
    saleSprint = models.FloatField(default=0.0)
    saleHour = models.FloatField(default=0.0)


    def __str__(self):
        return str(self.priceList) + ' - ' + str(self.resourceType)


    #Getters


    def getPriceList(self):
        return self.priceList


    def getResourceType(self):
        return self.resourceType


    def getCost(self):
        return self.cost


    def getSaleSprint(self):
        return self.saleSprint


    def getSaleHour(self):
        return self.saleHour


    #Setters


    def setPriceList(self, arg):
        if type(arg) is PriceList:
            self.priceList = arg
            self.save()
    

    def setResourceType(self, arg):
        if type(arg) is ResourceType:
            self.resourceType = arg
            self.save()


    def setCost(self, arg):
        try:
            arg = float(arg)
            self.cost = arg
            self.save()
        except:
            pass


    def setSaleSprint(self, arg):
        try:
            arg = float(arg)
            self.saleSprint = arg
            self.save()
        except:
            pass


    def setSaleHour(self, arg):
        try:
            arg = float(arg)
            self.saleHour = arg
            self.save()
        except:
            pass


class Company(models.Model):
    name = models.CharField(max_length=50, unique=True)
    proicereShare = models.FloatField(default=0.0)


    def __str__(self):
        return self.name


    def __eq__(self, other):
        return type(other) == Company and other.getName() == self.getName()


    def calculateSalesUSD(self, year):
        projects = Project.objects.filter(company=self, dealDate__range=(date(year,1,1), date(year,12,31) ))
        salesUSD = 0.0
        for p in projects:
            if p.getPriceList().getCurrency().getAcronym() == 'USD':
                salesUSD += p.getSalesPrice()
        return salesUSD


    def calculateSalesUSDtoARS(self, year):
        projects = Project.objects.filter(company=self, dealDate__range=(date(year,1,1), date(year,12,31) ),
        priceList__in=
            PriceList.objects.filter(currency__in=
            Currency.objects.filter(acronym='USD') ) )

        collections = Collection.objects.filter(project__in=projects)

        salesUSDtoARS = 0.0
        for c in collections:
            salesUSDtoARS += ( c.getAmount() * c.getExchangeRate() )

        return salesUSDtoARS


    def calculateSalesARS(self, year):
        projects = Project.objects.filter(company=self, dealDate__range=(date(year,1,1), date(year,12,31)))
        salesARS = 0.0
        for p in projects:
            if p.getPriceList().getCurrency().getAcronym() == 'ARS':
                salesARS += p.getSalesPrice()
        return salesARS


    def calculateCostARS(self, year):
        projects = Project.objects.filter(company=self, dealDate__range=(date(year,1,1), date(year,12,31)))
        cost = 0.0
        for p in projects:
            cost += p.calculateCostARS()
        return cost


    #Getters


    def getName(self):
        return self.name


    def getProicereShare(self):
        return self.proicereShare


    #Setters


    def setName(self, arg):
        self.name = str(arg)
        self.save()


    def setProicereShare(self, arg):
        try:
            arg = float(arg)
            self.proicereShare = arg
            self.save()
        except:
            pass



class Client(models.Model):
    name = models.CharField(max_length=50)


    def __str__(self):
        return self.name

    
    def __eq__(self, other):
        return type(other) == Client and self.name == other.getName()


    #Getters


    def getName(self):
        return self.name


    def grossSales(self, projects, acronym, year):
        sales = 0.0
        salesARS = 0.0
        for p in projects:
            sales += p.getSalesPrice()
            salesARS += ( p.getSalesPrice() * p.getExchangeRate() )

        return sales, salesARS


    def costARS(self, projects, acronym, year):
        cost = 0.0
        for p in projects:
            cost += ( p.calculateCostARS() + p.calculateTaxes() )

        return cost


    def collected(self, projects, acronym, year):
        collected = 0.0
        for p in projects:
            collected += p.calculateCollected()

        return collected


    def pending(self, projects, acronym, year):
        pending = 0.0
        for p in projects:
            pending += p.calculatePending()

        return pending


    def projectedProfit(self, projects, acronym, year):
        projected = 0.0
        for p in projects:
            projected += p.calculateProjectedProfit()

        return projected


    def netProfit(self, projects, acronym, year):
        net = 0.0
        for p in projects:
            net += p.calculateNetProfit()

        return net



class Resource(models.Model):
    resourceType = models.ForeignKey('ResourceType', on_delete=models.CASCADE, null=True)
    person = models.ForeignKey('Person', on_delete=models.CASCADE, null=True)
    company = models.ForeignKey('Company', on_delete=models.CASCADE, null=True)


    def __str__(self):
        return self.resourceType.getName() + " (" + str(self.person) + " for " + str(self.company) + ")"


    #Getters


    def getResourceType(self):
        return self.resourceType


    def getPerson(self):
        return self.person
    

    def getCompany(self):
        return self.company


    #Setters


    def setResourceType(self, arg):
        if type(arg) is ResourceType:
            self.resourceType = arg
            self.save()


    def setPerson(self, arg):
        if type(arg) is Person:
            self.person = arg
            self.save()


    def setCompany(self, arg):
        if type(arg) is Company:
            self.company = arg
            self.save()


class ResourceType(models.Model):
    name = models.CharField(max_length=50)


    def __str__(self):
        return self.name


    def __eq__(self, other):
        return type(other) == ResourceType and self.name == other.getName()


    #Getters


    def getName(self):
        return self.name


    #Setters


    def setName(self, arg):
        self.name = str(arg)
        self.save() 


class Person(models.Model):
    company = models.ForeignKey('Company', on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    birthdate = models.DateField()
    DNI = models.PositiveIntegerField()
    currentlyWorksHere = models.BooleanField(default=True)


    def __str__(self):
        return self.name + ' ' + self.surname


    #Getters


    def getName(self):
        return self.name

    
    def getSurname(self):
        return self.surname


    def getCompany(self):
        return self.company


class LabourBreakdown(models.Model):
    project = models.ForeignKey('Project', on_delete=models.CASCADE, null=True)
    resource = models.ForeignKey('Resource', on_delete=models.CASCADE, null=True)
    hoursWorked = models.PositiveIntegerField(default=0)
    facturacionEmpleado = models.PositiveIntegerField(default=1)

    
    def __str__(self):
        return str(self.resource) + ' in ' + str(self.project)


    def calculateFinalCostARS(self):
        fe = self.getFacturacionEmpleado()
        finalCost = 0.0
        if fe == 1:
            finalCost = self.calculateCost()
        elif fe == 2:
            finalCost = self.calculateSaleSprint()
        elif fe == 3:
            finalCost = self.calculateSaleHour()
        if self.getProject().getPriceList().getCurrency().getAcronym() != 'ARS':
            finalCost *= self.getProject().getExchangeRate()
        return finalCost


    def calculateCost(self):
        cost = 0.0
        priceList = self.project.getPriceList()
        resourceType = self.resource.getResourceType()
        if PriceListDetail.objects.filter(priceList=priceList, resourceType=resourceType).exists():
            listPrice = PriceListDetail.objects.get(priceList=priceList, resourceType=resourceType).getCost()
            cost = listPrice * self.hoursWorked
        return cost

    
    def calculateCostARS(self):
        if self.getProject().getPriceList().getCurrency().getAcronym() == 'USD':
            costARS = self.calculateCost() * self.project.getExchangeRate()
        else:
            costARS = self.calculateCost()
        return costARS


    def calculateSaleSprint(self):
        priceList = self.project.getPriceList()
        resourceType = self.resource.getResourceType()
        listPrice = PriceListDetail.objects.get(priceList=priceList, resourceType=resourceType).getSaleSprint()
        return listPrice * self.hoursWorked


    def calculateSaleSprintARS(self):
        if self.getProject().getPriceList().getCurrency().getAcronym() == 'USD':
            saleSprintARS = self.calculateSaleSprint() * self.project.getExchangeRate()
        else:
            saleSprintARS = self.calculateSaleSprint()
        return saleSprintARS


    def calculateSaleHour(self):
        priceList = self.project.getPriceList()
        resourceType = self.resource.getResourceType()
        listPrice = PriceListDetail.objects.get(priceList=priceList, resourceType=resourceType).getSaleHour()
        return listPrice * self.hoursWorked

    def calculateSaleHourARS(self):
        if self.getProject().getPriceList().getCurrency().getAcronym() == 'USD':
            saleHourARS = self.calculateSaleHour() * self.project.getExchangeRate()
        else:
            saleHourARS = self.calculateSaleHour()
        return saleHourARS


    #Getters

    def getProject(self):
        return self.project


    def getResource(self):
        return self.resource


    def getHoursWorked(self):
        return self.hoursWorked


    def getFacturacionEmpleado(self):
        return self.facturacionEmpleado


    #Setters


    def setProject(self, arg):
        if type(arg) is Project:
            self.project = arg
            self.save()


    def setResource(self, arg):
        if type(arg) is Resource:
            self.resource = arg
            self.save()


    def setHoursWorked(self, arg):
        try:
            hoursWorked = int(arg)
            self.hoursWorked = hoursWorked
            self.save()
        except:
            pass


    def setFacturacionEmpleado(self, arg):
        try:
            facturacionEmpleado = int(arg)
            if facturacionEmpleado in range(1,4):
                self.facturacionEmpleado = facturacionEmpleado
                self.save()
        except:
            pass

