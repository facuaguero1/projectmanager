from django.shortcuts import render
from django.views.generic import ListView
from .models import *
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from datetime import *

# Create your views here.


@login_required
def newProject(request):
    clients = Client.objects.all()
    priceLists = PriceList.objects.all()
    companies = Company.objects.all()

    if 'name' in request.POST:
        newName = request.POST['name']
        newCompany = Company.objects.get(pk=int(request.POST["company"]))
        newClient = Client.objects.get(pk=int(request.POST["client"]))
        newDealDate = request.POST['dealDate']
        print(request.POST['dealDate'])
        newSalesPrice = request.POST["salesPrice"]
        newPriceList = PriceList.objects.get(pk=int(request.POST["priceList"]))
        newExchangeRate = request.POST["exchangeRate"]

        project = Project()

        if newDealDate != "":
            project.setDealDate(newDealDate)

        project.setName(newName)
        project.setCompany(newCompany)
        project.setClient(newClient)
        project.setSalesPrice(newSalesPrice)
        project.setPriceList(newPriceList)
        project.setExchangeRate(newExchangeRate)

    return render(request, 'new_project.html', {'clients':clients, 'priceLists': priceLists, 'companies': companies})

@login_required
def projectList(request):
    year = datetime.now().year
    years = list( range(2016, (datetime.now().year + 1) ))
    if request.POST:
        year = int(request.POST['year'])

    projects = Project.objects.filter(dealDate__range=(date(year,1,1), date(year,12,31) ) )
    currencies = Currency.objects.all()
    collected = []
    for p in projects:
        collected.append(p.calculateCollected())
    
    projectsData = list(zip(projects, collected))

    rowspan = len(projectsData) + 2

    username = request.user.get_username()
    
    return render(request, 'projects_list.html', { 'rowspan': rowspan,'projectsData': projectsData, 'currencies': currencies, "username": username, "years": years, 'year': year})

@login_required
def modifyProject(request):
    username = request.user.get_username()
    try:
        projectId = int(request.POST['project'])
        project = Project.objects.get(pk=projectId)
        clients = Client.objects.all()
        priceLists = PriceList.objects.all()
        companies = Company.objects.all()
    except Exception:
        print(Exception)
        raise Http404
    

    if 'name' in request.POST:
        newName = request.POST['name']
        newCompany = Company.objects.get(pk=int(request.POST["company"]))
        newClient = Client.objects.get(pk=int(request.POST["client"]))
        newDealDate = request.POST['dealDate']
        newSalesPrice = request.POST["salesPrice"]
        newPriceList = PriceList.objects.get(pk=int(request.POST["priceList"]))
        newExchangeRate = request.POST["exchangeRate"]

        if newDealDate != "":
            project.setDealDate(newDealDate)


        project.setName(newName)
        project.setCompany(newCompany)
        project.setClient(newClient)
        project.setSalesPrice(newSalesPrice)
        project.setPriceList(newPriceList)
        project.setExchangeRate(newExchangeRate)
    
    return render(request, 'modify_project.html', {'username':username, 'project': project, 'clients':clients, 'priceLists': priceLists, 'companies': companies})

@require_http_methods(["POST"])
@login_required
def collectionsList(request):
    try:
        projectId = int(request.POST['project'])
        project = Project.objects.get(pk=projectId)
        collections = Collection.objects.filter(project= project)

        username = request.user.get_username()
    except:
        raise Http404

    return render(request, 'collections_list.html', {'collections': collections, 'project': project, 'username': username})


@require_http_methods(["POST"])
@login_required
def newCollection(request):
    projectId = int(request.POST['project'])
    project = Project.objects.get(pk=projectId)
        

    if 'cEntity' in request.POST:
        newCEntity = request.POST['cEntity']
        newSettlementDate = request.POST['settlementDate']
        newAmount = request.POST['amount']
        newExchangeRate = request.POST['exchangeRate']
        newSourceCommision = request.POST['sourceCommision']
        newCommisionARS = request.POST['commisionARS'] 
        newTaxesARS = request.POST['taxesARS']

        collection = Collection()

        if newSettlementDate != "":
            collection.setSettlementDate(newSettlementDate)

        collection.setProject(project)
        collection.setCEntity(newCEntity)
        collection.setAmount(newAmount)
        collection.setExchangeRate(newExchangeRate)
        collection.setSourceCommision(newSourceCommision)
        collection.setCommisionARS(newCommisionARS)
        collection.setTaxesARS(newTaxesARS)



    return render(request, 'new_collection.html', {'project': project})

@login_required
def modifyCollection(request):
    collectionId = int(request.POST['collection'])
    collection = Collection.objects.get(pk=collectionId)
    projectId = int(request.POST['project'])
    project = Project.objects.get(pk=projectId)

    if 'cEntity' in request.POST:
        newCEntity = request.POST['cEntity']
        newSettlementDate = request.POST['settlementDate']
        newAmount = request.POST['amount']
        newExchangeRate = request.POST['exchangeRate']
        newSourceCommision = request.POST['sourceCommision']
        newCommisionARS = request.POST['commisionARS']
        newTaxesARS = request.POST['taxesARS']

        if newSettlementDate != "":
            collection.setSettlementDate(newSettlementDate)

        collection.setProject(project)
        collection.setCEntity(newCEntity)
        collection.setAmount(newAmount)
        collection.setExchangeRate(newExchangeRate)
        collection.setSourceCommision(newSourceCommision)
        collection.setCommisionARS(newCommisionARS)
        collection.setTaxesARS(newTaxesARS)

    return render(request, 'modify_collection.html', {'collection': collection, 'project': project})


@require_http_methods(['POST'])
@login_required
def projectDetail(request):
    try:
        if 'delete' in request.POST:
            oid = int(request.POST['delete'])
            toDelete = LabourBreakdown.objects.get(id=oid)
            toDelete.delete()
            print('------------OBJECT DELETED------------')
        projectId = int(request.POST['project'])
        project = Project.objects.get(pk=projectId)

        username = request.user.get_username()

        labourBreakdownElements = LabourBreakdown.objects.filter(project=project)
        
        lbcost = []
        for element in labourBreakdownElements:
            tipoFacturacion = element.getFacturacionEmpleado()
            cost = 0.0
            if PriceListDetail.objects.filter(priceList=project.getPriceList(), resourceType=element.getResource().getResourceType()).exists():
                if tipoFacturacion == 1:
                    cost = PriceListDetail.objects.get(priceList=project.getPriceList(), resourceType=element.getResource().getResourceType()).getCost()
                elif tipoFacturacion == 2:
                    cost = PriceListDetail.objects.get(priceList=project.getPriceList(), resourceType=element.getResource().getResourceType()).getSaleSprint()
                elif tipoFacturacion == 3:
                    cost = PriceListDetail.objects.get(priceList=project.getPriceList(), resourceType=element.getResource().getResourceType()).getSaleHour()    
            lbcost.append(cost)

        lbprices = zip(labourBreakdownElements, lbcost)
        print("LABOR BREAKDOWN PRICES", lbprices)

    except Exception as e:
        print(e)

    return render(request, 'project_detail.html', {'username': username, 'project': project, 'lb': lbprices})


@require_http_methods(['POST'])
@login_required
def newLabourBreakdown(request):
    try:
        projectId = int(request.POST['project'])
        project = Project.objects.get(pk=projectId)
        if 'hoursWorked' in request.POST:
            newProject = project
            newResourceID = int(request.POST['resource'])
            newResource = Resource.objects.get(id=newResourceID)
            hoursWorked = float(request.POST['hoursWorked'])
            newHoursWorked = int(hoursWorked)
            newFacturacionEmpleado = request.POST['facturacionEmpleado']

            newLaborBreakdown = LabourBreakdown()

            newLaborBreakdown.setProject(newProject)
            newLaborBreakdown.setResource(newResource)
            newLaborBreakdown.setHoursWorked(newHoursWorked)
            newLaborBreakdown.setFacturacionEmpleado(newFacturacionEmpleado)

        rawResourceList = Resource.objects.all()
        lb = LabourBreakdown.objects.filter(project=project)
        usedResources = []
        resources = []
        for i in lb:
            usedResources.append(i.getResource())
        
        priceListDetails = PriceListDetail.objects.filter(priceList=project.getPriceList())
        availableResourceTypes = []
        for priceListDetail in priceListDetails:
            availableResourceTypes.append(priceListDetail.getResourceType())
        print('Available resource types:' , availableResourceTypes)

        for r in rawResourceList:
            resourceTypeIsInPriceList = r.getResourceType() in availableResourceTypes

            if r not in usedResources and resourceTypeIsInPriceList:
                resources.append(r)
                print('------------THIS CODE HAS BEEN RAN------------')


        print(rawResourceList)
        print(lb)
        print(usedResources)
        print(resources)

    except Exception as e:
        print(e)
        print("HA OCURRIDO UN ERROR JODER TIO")

    return render(request, 'new_labourbreakdown.html', {'project': project, 'resources': resources})

    
@login_required
def priceListsList(request):
    priceLists = PriceList.objects.all()
    username = request.user.get_username()

    return render(request, 'price_lists.html', {'priceLists': priceLists, 'username': username})


@require_http_methods(['POST'])
@login_required
def priceListDetail(request):
    priceListId = int(request.POST['priceList'])
    username = request.user.get_username()
    priceList = PriceList.objects.get(id=priceListId)
    priceListDetails = PriceListDetail.objects.filter(priceList=priceList)

    return render(request, 'price_list_detail.html', {'username': username, 'priceListDetails': priceListDetails, 'priceList': priceList})


@require_http_methods(['POST'])
@login_required
def modifyPricelist(request):
    priceListId = int(request.POST['priceList'])
    priceList = PriceList.objects.get(id=priceListId)
    if 'currency' in request.POST:
        newName = request.POST['name']
        newDescription = request.POST['description']
        currencyId = int(request.POST['currency'])
        newCurrency = Currency.objects.get(id=currencyId)
        newStartDate = request.POST['startDate']
        newEndDate = request.POST['endDate']

        if request.POST['startDate'] and request.POST['endDate']:
            if newStartDate != '':
                priceList.setStartDate(newStartDate)

            if newEndDate != '':
                priceList.setEndDate(newEndDate)

        priceList.setName(newName)
        priceList.setDescription(newDescription)
        priceList.setCurrency(newCurrency)

    currencies = Currency.objects.all()
    return render(request, 'modify_price_list.html', {'prl': priceList, 'currencies': currencies})


@login_required
def newPricelist(request):
    if 'currency' in request.POST:

        newName = request.POST['name']
        newDescription = request.POST['description']
        currencyId = int(request.POST['currency'])
        newCurrency = Currency.objects.get(id=currencyId)
        newStartDate = request.POST['startDate']
        newEndDate = request.POST['endDate']
        print(newStartDate)
        print(newEndDate)

        try:
            priceList = PriceList()

            priceList.setName(newName)
            
            if newStartDate != '':
                priceList.setStartDate(newStartDate)
            if newEndDate != '':
                priceList.setEndDate(newEndDate)

            priceList.setName(newName)
            priceList.setDescription(newDescription)
            priceList.setCurrency(newCurrency)
        except Exception as e:
            print(e)

    currencies = Currency.objects.all()

    return render(request, 'new_pricelist.html', {'currencies': currencies})


@require_http_methods(['POST'])
@login_required
def newPriceListDetail(request):
    priceListId = int(request.POST['priceList'])
    priceList = PriceList.objects.get(id=priceListId)

    if 'cost' in request.POST:
        newResourceTypeId = int(request.POST['resourceType'])
        newResourceType = ResourceType.objects.get(id=newResourceTypeId)
        newCost = float(request.POST['cost'])
        newSaleSprint = float(request.POST['saleSprint'])
        newSaleHour = float(request.POST['saleHour'])

        priceListDetail = PriceListDetail()

        priceListDetail.setPriceList(priceList)
        priceListDetail.setResourceType(newResourceType)
        priceListDetail.setCost(newCost)
        priceListDetail.setSaleSprint(newSaleSprint)
        priceListDetail.setSaleHour(newSaleHour)


    priceListDetails = PriceListDetail.objects.filter(priceList=priceList)
    pricedResourceTypes = []
    for pld in priceListDetails:
        pricedResourceTypes.append(pld.getResourceType())

    resourceTypesRaw = ResourceType.objects.all()
    resourceTypes = []
    for rt in resourceTypesRaw:
        if rt not in pricedResourceTypes:
            resourceTypes.append(rt)

    return render(request, 'new_pricelist_detail.html', {'priceList': priceList, 'resourceTypes': resourceTypes} )


@require_http_methods(['POST'])
@login_required
def modifyPriceListDetail(request):
    priceListDetailId = int(request.POST['priceListDetail'])
    priceListDetail = PriceListDetail.objects.get(id=priceListDetailId)
    priceList = priceListDetail.getPriceList()

    if 'cost' in request.POST:
        newCost = float(request.POST['cost'])
        newSaleSprint = float(request.POST['saleSprint'])
        newSaleHour = float(request.POST['saleHour'])

        priceListDetail.setCost(newCost)
        priceListDetail.setSaleSprint(newSaleSprint)
        priceListDetail.setSaleHour(newSaleHour)


    return render(request, 'modify_pricelist_detail.html', {'priceListDetail': priceListDetail, 'priceList': priceList} )


@login_required
def dashboard(request):
    username = request.user.get_username()
    year = datetime.now().year
    years = list( range(2016, (datetime.now().year + 1) ) )
    if request.POST:
        year = int(request.POST['year'])
    companies = Company.objects.all()

    #Gross sales/company & gross margin/company table and piecharts

    class Row:
        def __init__(self, company):
            self.company = company.getName()
            self.salesUSD = company.calculateSalesUSD(year)
            self.salesUSDtoARS = company.calculateSalesUSDtoARS(year)
            self.salesARS = company.calculateSalesARS(year)
            self.totalARS = self.salesUSDtoARS + self.salesARS
            self.cost = company.calculateCostARS(year)
            self.grossMargin = self.totalARS - self.cost
            if self.totalARS > 0:
                self.PercentageGrossMargin = (self.grossMargin / self.totalARS) * 100
            else:
                self.PercentageGrossMargin = 0.0
            self.gmProicere = self.grossMargin * company.proicereShare
            self.PercentageGmProicere = self.PercentageGrossMargin * company.proicereShare


    rows = []
    for com in companies:
        rows.append(Row(com))
    
    grossSalesGraphicData = "['Company', 'Gross sales'],"
    for r in rows:
        grossSalesGraphicData += '["' + str(r.company) + '", ' + str(r.totalARS) + "], "


    grossMarginGraphicData = "['Company', 'Gross margin'],"
    for r in rows:
        grossMarginGraphicData += '["' + str(r.company) + '", ' + str(r.gmProicere) + "], "


    #Interanual gross sales table and columns graphic

    class Row2:
        def __init__(self, company):
            self.company = company.getName()
            self.totalLastYear = company.calculateSalesUSDtoARS(year-1) + company.calculateSalesARS(year-1)
            self.totalThisYear = company.calculateSalesUSDtoARS(year) + company.calculateSalesARS(year)

    rows2 = []
    for com in companies:
        rows2.append(Row2(com))

    grossSalesLastYear = 0.0
    grossSalesThisYear = 0.0
    for r in rows2:
        grossSalesLastYear += r.totalLastYear
        grossSalesThisYear += r.totalThisYear

    interanualGrossSalesGraphicData = '["Year", "Gross sales", ' + '{ role: "style" }' + '], ["' + str(year-1) + '", ' + str(grossSalesLastYear) + ', "blue"' + '], [" ' + str(year) + '", ' + str(grossSalesThisYear) + ',"blue"' + "],"

    #Interanual gross sale by company columns graphic

    interanualGrossSalesByCompanyGraphicData = "['Year', "
    for r in rows2:
        interanualGrossSalesByCompanyGraphicData += "'" + r.company + "', "
    interanualGrossSalesByCompanyGraphicData += "],"

    for y in range((year-1),(year+1)):
        interanualGrossSalesByCompanyGraphicData += "['" + str(y) + "',"
        if y == year-1:
            for r in rows2:
                interanualGrossSalesByCompanyGraphicData += str(r.totalLastYear) + ", "
        else:
            for r in rows2:
                interanualGrossSalesByCompanyGraphicData += str(r.totalThisYear) + ", "
        interanualGrossSalesByCompanyGraphicData += "], "

    return render(request, 'dashboard.html', {'username': username, 'rows': rows, 'grossSalesLastYear': grossSalesLastYear, 'grossSalesThisYear': grossSalesThisYear, 'rows2': rows2, 'year': year, 'lastYear': year-1, 'years': years, 'grossSalesGraphicData': grossSalesGraphicData, 'grossMarginGraphicData': grossMarginGraphicData, 'interanualGrossSalesGraphicData': interanualGrossSalesGraphicData, 'interanualGrossSalesByCompanyGraphicData': interanualGrossSalesByCompanyGraphicData})


@login_required
def summary(request):
    username = request.user.get_username()
    year = datetime.now().year
    years = list( range(2016, (datetime.now().year + 1) ) )
    if request.POST:
        year = int(request.POST['year'])

    class Row2:
        def __init__(self, client, acronym, year):
            projects = Project.objects.filter(client=client, dealDate__range=(date(year,1,1), date(year,12,31) ),
        priceList__in= PriceList.objects.filter(currency__in=Currency.objects.filter(acronym=acronym) ) )

            self.client = client.getName()
            self.currency = acronym
            self.grossSaleRaw = client.grossSales(projects, acronym, year)
            self.grossSale = self.grossSaleRaw[0]
            self.grossSaleARS = self.grossSaleRaw[1]
            self.costARS = client.costARS(projects, acronym, year)
            self.collected = client.collected(projects, acronym, year)
            self.pending = client.pending(projects, acronym, year)
            self.projectedProfit = client.projectedProfit(projects, acronym, year)
            self.netProfit = client.netProfit(projects, acronym, year)

    clients = Client.objects.all()
    rows = []
    for client in clients:
        for curr in ["USD", "ARS"]:
            row = Row2(client, curr, year)
            rows.append(row)
    
    annualGrossSales = 0.0
    totalCost = 0.0
    netProfit = 0.0
    collectedUSD = 0.0
    collectedARS = 0.0
    pendingUSD = 0.0
    pendingARS = 0.0

    for row in rows:
        annualGrossSales += row.grossSaleARS
        totalCost += row.costARS
        netProfit += row.netProfit
        if row.currency == "USD":
            collectedUSD += row.collected
            pendingUSD += row.pending
        elif row.currency == "ARS":
            collectedARS += row.collected
            pendingARS += row.pending


    return render(request, 'summary.html', {'username': username,
                                            'year': year,
                                            'years': years,
                                            'rows': rows,
                                            'annualGrossSales': annualGrossSales,
                                            'totalCost': totalCost,
                                            'netProfit': netProfit,
                                            'collectedUSD': collectedUSD,
                                            'collectedARS': collectedARS,
                                            'pendingUSD': pendingUSD,
                                            'pendingARS': pendingARS })

@login_required
def index(request):
    username = request.user.get_username()
    return render(request, 'index.html', {'username': username})
