"""projectmanager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from django.conf.urls import url
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views

from apps.main.views import *
from apps.users.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^projects/$', projectList),
    url(r'^$', index),
    url(r'^pricelists/$', priceListsList),
    url(r'^nuevo_proyecto/$', newProject),
    url(r'^modificar_proyecto/$', modifyProject),
    url(r'accounts/login/$', auth_views.login, name='login'),
    url(r'^accounts/logout/$', auth_views.logout, name='logout'),
    url(r'^collections/$', collectionsList),
    url(r'^nueva_collection/$', newCollection),
    url(r'^modificar_collection/', modifyCollection),
    url(r'^project_detail/$', projectDetail),
    url(r'^nuevo_laborbreakdown/$', newLabourBreakdown),
    url(r'^pricelists/new/$', newPricelist),
    url(r'^pricelists/modificar/$', modifyPricelist),
    url(r'^pricelists/detail/$', priceListDetail),
    url(r'^pricelists/detail/new/$', newPriceListDetail),
    url(r'^pricelists/detail/modificar/$', modifyPriceListDetail),
    url(r'^dashboard/$', dashboard),
    url(r'^summary/$', summary),
]
